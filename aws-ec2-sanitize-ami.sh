#!/usr/bin/env bash
#set -ex

# Usage: bash <(curl -sL https://gist.github.com/thimslugga/<>/sanitze-clean.sh)
#
# https://docs.redhat.com/en/documentation/red_hat_enterprise_linux/8/html-single/configuring_and_managing_cloud-init_for_rhel_8/index
# https://docs.aws.amazon.com/imagebuilder/latest/userguide/security-best-practices.html
# https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/building-shared-amis.html
# https://github.com/aws/amazon-ecs-ami/blob/main/scripts/cleanup.sh
# https://github.com/awslabs/amazon-eks-ami/blob/main/templates/shared/provisioners/cleanup.sh

function info() {
  echo -e "\e[32m${1}\e[0m"
}

function cleanup() {
  local files
  files=("$@")
  
  for f in "${files[@]}"; do
    if [[ -f "${f}" ]]; then
      info "Deleting ${f}";
      shred -zuf "${f}";
    fi;
    
    if [[ -f "${f}" ]]; then
      info "Failed to delete '${f}'. Aborting."
      #exit 1;
    fi;
  done
};

FILES=(
  "/root/anaconda-ks.cfg"
  "/root/original-ks.cfg"
  "/etc/hostname"
  "/etc/locale.conf"
  "/root/.ssh/authorized_keys"
  "/home/ec2-user/.ssh/authorized_keys"
  "/home/ec2-user/.bash_history"
  "/home/ubuntu/.ssh/authorized_keys"
  "/home/ubuntu/.bash_history"
  "/home/cloud-user/.ssh/authorized_keys"
  "/home/cloud-user/.bash_history"
  "/etc/ssh/ssh_host_rsa_key"
  "/etc/ssh/ssh_host_rsa_key.pub"
  "/etc/ssh/ssh_host_ecdsa_key"
  "/etc/ssh/ssh_host_ecdsa_key.pub"
  "/etc/ssh/ssh_host_ed25519_key"
  "/etc/ssh/ssh_host_ed25519_key.pub"
  "/etc/udev/rules.d/70-persistent-net.rules"
  "/etc/netplan/50-cloud-init.yaml"
  "/etc/sudoers.d/90-cloud-init-users"
  "/etc/.updated"
  "/etc/aliases.db"
  "/var/lib/misc/postfix.aliasesdb-stamp"
  "/var/lib/postfix/master.lock"
  "/var/spool/postfix/pid/master.pid"
  "/var/.updated"
  "/var/cache/yum/x86_64/2/.gpgkeyschecked.yum"
  "/var/log/cloud-init.log"
  "/var/log/cloud-init-output.log"
  "/var/log/ubuntu-advantage.log"
  "/var/log/audit/audit.log"
  "/var/log/boot.log"
  "/var/log/dmesg"
  "/var/log/messages"
  "/var/log/cron"
)

# wait for cloud-init to finish
cloud-init status --wait

info 'Cleanup cloud-init metadata'
cloud-init clean --logs --seed

info 'Clean Apt'
apt-get -y autoremove
apt-get -y clean

info 'Clean Yum/DNF'
yum clean all
rm -rf /var/cache/yum

info 'Remove SSH public keys'
[ -f /home/ec2-user/.ssh/authorized_keys ] && rm -f /home/ec2-user/.ssh/authorized_keys
[ -f /home/ubuntu/.ssh/authorized_keys ] && rm -f /home/ubuntu/.ssh/authorized_keys
[ -f /home/cloud-user/.ssh/authorized_keys ] && rm -f /home/cloud-user/.ssh/authorized_keys

info  'Cleanup log files'
find /var/log -type f \
  | while read f; do 
    echo -ne '' > $f; 
  done

info 'Cleanup bash history'
unset HISTFILE
[ -f /root/.bash_history ] && rm -f /root/.bash_history
[ -f /home/ec2-user/.bash_history ] && rm -f /home/ec2-user/.bash_history
[ -f /home/ubuntu/.bash_history ] && rm -f /home/ubuntu/.bash_history
[ -f /home/cloud-user/.bash_history ] && rm -f /home/cloud-user/.bash_history

# Ubuntu
#[ -f /var/log/ubuntu-advantage.log ] && rm -f /var/log/ubuntu-advantage.log
#[ -f /etc/netplan/50-cloud-init.yaml ] && rm -f /etc/netplan/50-cloud-init.yaml
#[ -f /etc/udev/rules.d/70-persistent-net.rules ] && rm -f /etc/udev/rules.d/70-persistent-net.rules

rm -rf \
    /etc/machine-id \
    /var/cache/{dnf,yum} \
    /etc/{dnf,yum}/vars/{awsregion,awsdomain}
    /var/lib/dhcp/dhclient.* \
    /var/lib/{yum,dnf}/history* \
    /var/log/{wtmp,secure} \
    /etc/ssh/ssh_host* \
    /tmp/*

info 'Ensure /etc/machine-id exists'
truncate -s 0 /etc/machine-id
truncate -s 0 /var/lib/dbus/machine-id
[ ! -f /etc/machine-id ] && touch /etc/machine-id

rm -rf /var/lib/cloud/

#dd if=/dev/zero of=/EMPTY bs=1M && rm -f /{EMPTY,zeros}

info 'The instance has been sanitized!'